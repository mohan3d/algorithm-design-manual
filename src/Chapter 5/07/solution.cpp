#include <iostream>

using namespace std;

template<class T>
class Node{
public:
    T value;
    Node<T> *left;
    Node<T> *right;

    Node(T _value): value(_value), left(nullptr), right(nullptr){}
    Node(T _value, Node<T>* _left): value(_value), left(_left), right(nullptr){}
    Node(T _value, Node<T>* _left, Node<T>* _right): value(_value), left(_left), right(_right){}
};

template<class T>
class TreeBuilderIP{
private:
    T* inorder;
    T* preorder;
    int size;
    int preorderIndex;

    int find(int target, int start, int end){        

        for(int i = start; i <= end; ++i)
            if(target == inorder[i])
                return i;       

        return -1;
    }

    Node<T>* build(int start, int end){
        if(start > end)
            return nullptr;

        // error if index == -1.
        int index = find(preorder[preorderIndex++], start, end);

        return new Node<T>(inorder[index],
                           build(start, index - 1),
                           build(index + 1, end));
    }

public:
    TreeBuilderIP(T inorder[], T preorder[], int size){
        this->inorder = inorder;
        this->preorder = preorder;
        this->size = size;
        this->preorderIndex = 0;
    }

    Node<T>* build(){
        return build(0, size - 1);
    }
};

template<class T>
void inorderTraversal(Node<T>* p){
    if(p != nullptr){
        inorderTraversal(p->left);
        cout << p->value << " ";
        inorderTraversal(p->right);
    }
}

template<class T>
void preorderTraversal(Node<T>* p){
    if(p != nullptr){
        cout << p->value << " ";
        preorderTraversal(p->left);
        preorderTraversal(p->right);
    }
}

template<class T>
void postorderTraversal(Node<T>* p){
    if(p != nullptr){
        postorderTraversal(p->left);
        postorderTraversal(p->right);
        cout << p->value << " ";
    }
}

int main() {

    const int size = 11;
    int inorder[] = {0, 1, 2, 3, 4, 7, 10, 15, 18, 20, 30};
    int preorder[] = {10, 7, 2, 1, 0, 3, 4, 20, 15, 18, 30};
    int postorder[] = {0, 1, 4, 3, 2, 7, 18, 15, 30, 20, 10};

    TreeBuilderIP<int> tbip(inorder, preorder, size);
    Node<int>* rootip = tbip.build();

    inorderTraversal(rootip);
    cout << endl;
    preorderTraversal(rootip);
    cout << endl;
    postorderTraversal(rootip);
    cout << endl;

//    // Left subTree
//    Node<int>* n0 = new Node<int>(0);
//    Node<int>* n1 = new Node<int>(1, n0);
//    Node<int>* n4 = new Node<int>(4);
//    Node<int>* n3 = new Node<int>(3, nullptr, n4);
//    Node<int>* n2 = new Node<int>(2, n1, n3);
//    Node<int>* n7 = new Node<int>(7, n2);
//    // Right subTree
//    Node<int>* n18 = new Node<int>(18);
//    Node<int>* n15 = new Node<int>(15, nullptr, n18);
//    Node<int>* n30 = new Node<int>(30);
//    Node<int>* n20 = new Node<int>(20, n15, n30);
//    // Tree
//    Node<int>* n10 = new Node<int>(10, n7, n20);
//
//    inorderTraversal(n10);
//    cout << endl;
//    preorderTraversal(n10);
//    cout << endl;
//    postorderTraversal(n10);
//    cout << endl;
//    levelTraversal(n10);
//    cout << endl;

    return 0;
}