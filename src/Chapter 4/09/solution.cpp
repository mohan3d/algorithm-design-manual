/*
    4-9. [4 ] Give an efficient algorithm to compute the union of sets A and B, where
    n = max(|A|, |B|). The output should be an array of distinct elements that form
    the union of the sets, such that they appear more than once in the union.
    (a) Assume that A and B are unsorted. Give an O(n log n) algorithm for the problem.
    (b) Assume that A and B are sorted. Give an O(n) algorithm for the problem.
*/

/*
  a) sort and use (merge sort merge function). O(n lg n)
  b) only use merge. O(n)
 */

#include <iostream>
#include <algorithm>
using namespace std;

int* Merge(int a[], int aSize, int b[], int bSize){
    int i, j, k;


    int *result = new int[aSize + bSize];

    int minSize = min(aSize, bSize);
    i = j = k = 0;

    while(k < minSize){
        if(a[i] < b[j])
            result[k++] = a[i++];
        else if(b[j] < a[i])
            result[k++] = b[j++];
        else {
            result[k++] = a[i];
            ++i; ++j;
        }
    }

    while(i < aSize)
        result[k++] = a[i++];

    while(j < bSize)
        result[k++] = b[j++];

    return result;
}

int* SortAndMerge(int a[], int aSize, int b[], int bSize){
    sort(a, a + aSize);
    sort(b, b + bSize);
    return Merge(a, aSize, b, bSize);
}


