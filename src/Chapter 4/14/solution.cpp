/*
    4-14. [5] Give an O(n log k)-time algorithm that merges k sorted lists with a total of n
    elements into one sorted list. (Hint: use a heap to speed up the elementary O(kn)-
    time algorithm).
*/
/*
 construct a heap with pairs (first element of each list, list number)
 while heap is not empty:
    min_element = pop min element
    add next element from the list which min_element is from
 */
#include <iostream>
#include <vector>
#include <queue>
using namespace std;


template <typename T>
vector<T> Merge(vector<vector<T>> lists){
    priority_queue<pair<T, int>, vector<pair<T, int>>, greater<pair<T, int>> > pq;
    vector<T> output;
    vector<int> indices(lists.size(), 1);

    // Initialize heap with pairs of (first element of list, index of this list)
    for (int i = 0; i < lists.size(); ++i) {
        pq.push(make_pair(lists[i][0], i));
    }

    while(!pq.empty()){
        pair<T, int> minimum = pq.top();
        pq.pop();

        int list_number = minimum.second;

        output.push_back(minimum.first);

        // If current list not empty (still having elements)
        if(indices[list_number] < lists[list_number].size()){

            // Add new element (top of current list, list number)
            pq.push(make_pair(lists[list_number][indices[list_number]], list_number));

            // Increment current list index.
            indices[list_number]++;
        }
    }

    return output;
}
