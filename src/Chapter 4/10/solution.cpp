/*
    4-10. [5] Given a set S of n integers and an integer T , give an O(n^k−1 log n) algorithm
    to test whether k of the integers in S add up to T .
*/

#include <iostream>
#include <algorithm>
using namespace std;

int binary_search(int a[], int start, int end, int element){
    int mid;

    while(start <= end){
        mid = start + (end - start)/2;
        if(a[mid] == element)
            return mid;
        else if(element < a[mid])
            end = mid - 1;
        else start = mid + 1;
    }
    return -1;
}

bool findSum(int a[], int size, int k, int index, int x){
    if(!k) return binary_search(a, index, size - 1, x) != -1;


    for(int i = index; i < size; ++i){
        if(findSum(a, size, k - 1, i + 1, x - a[i])) return true;
    }

    return false;
}

/*
  Sort O(n lg n).
  Generate n - 1 nested loop, then do a binary search. O(n^(k - 1) log n).
 */
bool kSum(int a[], int size, int k, int x){
    sort(a, a + size);
    return findSum(a, size, k - 1, 0, x);
}
