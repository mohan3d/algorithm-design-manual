/*
    4-4. [3] Assume that we are given n pairs of items as input, where the first item is a
    number and the second item is one of three colors (red, blue, or yellow). Further
    assume that the items are sorted by number. Give an O(n) algorithm to sort the
    items by color (all reds before all blues before all yellows) such that the numbers
    for identical colors stay sorted.
    For example: (1,blue), (3,red), (4,blue), (6,yellow), (9,red) should become (3,red),
    (9,red), (1,blue), (4,blue), (6,yellow).
*/

#include <iostream>
using namespace std;

enum Color{
    red, blue, yellow
};

struct Unit{
    int num;
    Color color;

    Unit(int _num, Color _color): num(_num), color(_color){}
    Unit(){}
};


void SortColors(Unit a[], int size){
    Unit *tmp = new Unit[size];
    int j = 0;

    for(int i = 0; i < size; ++i)
        if(a[i].color == red)
            tmp[j++] = a[i];

    for(int i = 0; i < size; ++i)
        if(a[i].color == blue)
            tmp[j++] = a[i];

    for(int i = 0; i < size; ++i)
        if(a[i].color == yellow)
            tmp[j++] = a[i];

    for(int i = 0; i < size; ++i){
        a[i] = tmp[i];
    }

    delete[] tmp;
}

int main() {
    const int size = 5;
    Unit a[] = {Unit(1, blue), Unit(3,red), Unit(4,blue), Unit(6,yellow), Unit(9,red)};
    SortColors(a, size);

    for (int i = 0; i < size; ++i) {
        cout << "(" << a[i].num << ", " << a[i].color << ")" << endl;
    }

    return 0;
}

