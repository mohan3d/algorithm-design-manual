/*
    4-20. [3] Give an efficient algorithm to rearrange an array of n keys so that all the
    negative keys precede all the non-negative keys. Your algorithm must be in-place,
    meaning you cannot allocate another array to temporarily hold the items. How fast
    is your algorithm?
*/

#include <iostream>
using namespace std;

// O(n)
void NegativePositive(int a[], const int size) {
    int i = 0;
    int j = size - 1;

    while(i < j){
        if(a[i] > 0){
            swap(a[i], a[j]);
            --j;
        }else{
            ++i;
        }
    }
}

int main() {
    const int size = 5;
    int a[] = {-1, 10, 3, -5, 20};
    NegativePositive(a, size);

    for (int i = 0; i < size; ++i) {
        cout << a[i] << endl;
    }

    return 0;
}


