/*
    4-18. [5] Suppose an array A consists of n elements, each of which is red, white, or blue.
    We seek to sort the elements so that all the reds come before all the whites, which
    come before all the blues The only operation permitted on the keys are
    • Examine(A,i) – report the color of the ith element of A.
    • Swap(A,i,j) – swap the ith element of A with the jth element.
    Find a correct and efficient algorithm for red-white-blue sorting. There is a linear-
    time solution.
*/

#include <iostream>
using namespace std;

enum Color{
    red, white, blue
};

/*
 * The only operation permitted on the keys.
 */
Color Examine(Color A[], int i){
    return A[i];
}

void Swap(Color A[], int i, int j){
    Color tmp = A[i];
    A[i] = A[j];
    A[j] = tmp;
}


void SortColors(Color a[], int size){

    int i = 0;
    int j = 0;
    int k = size - 1;

    Color c;

    while(j <= k){
        c = Examine(a, j);

        if(c == red){
            Swap(a, i, j);
            ++i; ++j;
        }else if(c == blue){
            Swap(a, j, k);
            --k;
        }else{
            ++j;
        }
    }
}

int main() {
    const int size = 5;
    Color a[] = {blue, red, blue, white, red};
    SortColors(a, size);

    for (int i = 0; i < size; ++i) {
        cout << a[i] << endl;
    }

    return 0;
}

