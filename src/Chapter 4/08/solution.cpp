/*
    4-8. [4 ] Given a set of S containing n real numbers, and a real number x. We seek an
    algorithm to determine whether two elements of S exist whose sum is exactly x.
    (a) Assume that S is unsorted. Give an O(n log n) algorithm for the problem.
    (b) Assume that S is sorted. Give an O(n) algorithm for the problem.
*/

#include <iostream>
#include <algorithm>
using namespace std;

int binary_search(int a[], int size, int element){
    int start = 0;
    int end = size - 1;
    int mid;

    while(start <= end){
        mid = start + (end - start)/2;
        if(a[mid] == element)
            return mid;
        else if(element < a[mid])
            end = mid - 1;
        else start = mid + 1;
    }
    return -1;
}


/*
    a) sort , loop through the array do a binary search (x - a[i]) against the array.
*/
void solutionA(int a[], int size, int x){
    sort(a, a + size);
    for (int i = 0; i < size; ++i) {
        int found = binary_search(a, size, x - a[i]);
        if(found != -1 && found != i){
            cout << i << " " << found << endl;
            return;
        }
    }
}

// a must be sorted array.
void solutionB(int a[], int size, int x){
    int start = 0;
    int end = size - 1;
    int sum;

    while(start < end){
        sum = a[start] + a[end];

        if(sum == x){
            cout << start << " " << end << endl;
            return;
        }else if(sum < x)
            ++start;
        else --end;
    }
}

