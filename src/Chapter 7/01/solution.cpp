/*
    7-1. [3] A derangement is a permutation p of {1, . . . , n} such that no item is in its proper
    position, i.e. p i  = i for all 1 ≤ i ≤ n. Write an efficient backtracking program with
    pruning that constructs all the derangements of n items.
*/

/*
 * Edit (7.1.2 Constructing All Permutations) solution.
 */
#include <iostream>

#define FALSE 0
#define TRUE 1
#define MAXCANDIDATES 100
#define NMAX 100

bool finished = FALSE;

template<typename data>
void make_move(int a[], int k, data input) { }

template<typename data>
void unmake_move(int a[], int k, data input) { }

template<typename data>
bool is_a_solution(int a[], int k, data n) { return (k == n); }

template<typename data>
void process_solution(int a[], int k, data input) {
    int i;
    for (i = 1; i <= k; i++) printf(" %d", a[i]);
    printf("\n");
}

template<typename data>
void construct_candidates(int a[], int k, data n, int c[], int *ncandidates) {
    int i;
    bool in_perm[NMAX];

    for (i = 1; i < NMAX; i++) in_perm[i] = FALSE;
    for (i = 0; i < k; i++) in_perm[a[i]] = TRUE;

    *ncandidates = 0;

    for (i = 1; i <= n; i++)
        if (in_perm[i] == FALSE && i != k) {    /* CHANGED */
            c[*ncandidates] = i;
            *ncandidates = *ncandidates + 1;
        }
}

template<typename data>
void backtrack(int a[], int k, data input) {
    int c[MAXCANDIDATES];
    int ncandidates;
    int i;

    if (is_a_solution(a, k, input))
        process_solution(a, k, input);
    else {
        k = k + 1;
        construct_candidates(a, k, input, c, &ncandidates);
        for (i = 0; i < ncandidates; i++) {
            a[k] = c[i];
            make_move(a, k, input);
            backtrack(a, k, input);
            unmake_move(a, k, input);
            if (finished) return;
        }
    }
}

void generate_derangements(int n) {
    int a[NMAX];
    backtrack(a, 0, n);
}

int main() {

    generate_derangements(4);
    return 0;
}

