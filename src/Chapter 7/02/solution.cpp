/*
    7-2. [4] Multisets are allowed to have repeated elements. A multiset of n items may
    thus have fewer than n! distinct permutations. For example, {1, 1, 2, 2} has only
    six different permutations: {1, 1, 2, 2}, {1, 2, 1, 2}, {1, 2, 2, 1}, {2, 1, 1, 2}, {2, 1, 2, 1},
    and {2, 2, 1, 1}. Design and implement an efficient algorithm for constructing all
    permutations of a multiset.
*/

/*
 * Edit (7.1.2 Constructing All Permutations) solution.
 */
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>


using namespace std;

#define FALSE 0
#define TRUE 1
#define MAXCANDIDATES 100
#define NMAX 100

bool finished = FALSE;
map<int, int> elements;     /* Hash Map can be used */
vector<int> keys;

template<typename data>
void make_move(int a[], int k, data input) { elements[a[k]]--; }

template<typename data>
void unmake_move(int a[], int k, data input) { elements[a[k]]++; }

template<typename data>
bool is_a_solution(int a[], int k, data n) { return (k == n); }

template<typename data>
void process_solution(int a[], int k, data input) {
    int i;
    for (i = 1; i <= k; i++) printf(" %d", a[i]);
    printf("\n");
}

template<typename data>
void construct_candidates(int a[], int k, data n, int c[], int *ncandidates) {
    int i = 0;

    for (int j = 0; j < keys.size(); ++j) {
        if (elements[keys[j]] > 0) {
            c[i++] = keys[j];
        }
    }

    *ncandidates = i;
}

template<typename data>
void backtrack(int a[], int k, data input) {
    int c[MAXCANDIDATES];
    int ncandidates;
    int i;

    if (is_a_solution(a, k, input))
        process_solution(a, k, input);
    else {
        k = k + 1;
        construct_candidates(a, k, input, c, &ncandidates);
        for (i = 0; i < ncandidates; i++) {
            a[k] = c[i];
            make_move(a, k, input);
            backtrack(a, k, input);
            unmake_move(a, k, input);
            if (finished) return;
        }
    }
}

void initialize(int data[], int size) {

    for(int i = 0; i < size; ++i){
        if(elements.find(data[i]) == elements.end()) {
            elements[data[i]] = 0;
            keys.push_back(data[i]);
        }

        elements[data[i]]++;
    }

    sort(keys.begin(), keys.end());
}

void generate_permutations(int data[], int n) {
    initialize(data, n);
    int a[NMAX];
    backtrack(a, 0, n);
}

int main() {

    const int n = 4;
    int data[n] = {1, 1, 2, 2};
    generate_permutations(data, n);

    return 0;
}

