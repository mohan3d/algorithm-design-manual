/*
 3-2. [3] Write a program to reverse the direction of a given singly-linked list. In other
 words, after the reversal all pointers should now point backwards. Your algorithm
 should take linear time.
*/

#include <iostream>

using namespace std;

template <typename T>
struct Node{
    T value;
    Node<T> *next;

    Node<T>(const T& _value, Node<T> *_next): value(_value), next(_next){}
    Node<T>(const T& _value) : Node(_value, nullptr){}
};

template <typename T>
void ReverseLinkedList(Node<T>** list, Node<T> *first, Node<T> *second){
    if(second == nullptr) {
        *list = first;
        return;
    }

    ReverseLinkedList(list, second, second->next);
    second->next = first;
}

Node<int>* CreateLinkedList(){
    Node<int> *n5 = new Node<int>(5);
    Node<int> *n4 = new Node<int>(4, n5);
    Node<int> *n3 = new Node<int>(3, n4);
    Node<int> *n2 = new Node<int>(2, n3);
    Node<int> *n1 = new Node<int>(1, n2);

    return n1;
}

template <typename T>
void TraverseLinkedList(Node<T> *list){
    Node<T> *tmp = list;

    while(tmp != nullptr) {
        cout << tmp->value << " ";
        tmp = tmp->next;
    }
}



int main() {
    Node<int> *list = CreateLinkedList();
    TraverseLinkedList(list);
    ReverseLinkedList(&list, (Node<int>*) nullptr, list);
    cout << endl;
    TraverseLinkedList(list);

    return 0;
}
