/*
    3-15. [8] Design a data structure that allows one to search, insert, and delete an integer
    X in O(1) time (i.e. , constant time, independent of the total number of integers
    stored). Assume that 1 ≤ X ≤ n and that there are m + n units of space available,
    where m is the maximum number of integers that can be in the table at any one
    time. (Hint: use two arrays A[1..n] and B[1..m].) You are not allowed to initialize
    either A or B, as that would take O(m) or O(n) operations. This means the arrays
    are full of random garbage to begin with, so you must be very careful.
*/

/*
 * Assume no duplicated insertions: http://www.algorithm.cs.sunysb.edu/algowiki/index.php/TADM2E_3.15
 */

// Assume duplicated insertions.
/*
 * Keep existing elements(count > 0) to the left of "index",
 * search checks for element existence (element to the left of index && unit.num == element),
 * delete decrements unit.count , if it became 0 (means this element is no longer available).
 */
#include <iostream>
using namespace std;

class DS{
private:
    struct unit{
        int num;
        int count;
    };

    int *A;
    unit *B;
    int index;

    unit createUnit(int element, int count=1){
        unit u;
        u.num = element;
        u.count = count;

        return u;
    }

public:
    DS(int n, int m){
        A = new int[n];
        B = new unit[m];
        index = 0;
    }

    ~DS(){
        delete[] A;
        delete[] B;
    }

    void insertElement(int element){
        if(search(element)){
            ++B[A[element]].count;
        }else{
            A[element] = index++;
            B[A[element]] = createUnit(element);
        }
    }

    void deleteElement(int element){
        if(search(element)){
            --B[A[element]].count;

            // count == 0
            if(!B[A[element]].count){
                swap(B[A[element]], B[--index]);
            }

            int swappedElement = B[A[element]].num;
            A[swappedElement] = A[element];
        }
        // else if element doesn't exist
    }

    bool search(int element){
        int mIndex = A[element];
        if(mIndex < index && mIndex >= 0){
            if(B[mIndex].num == element) {
                return true;
            }
        }

        return false;
    }
};


int main() {


    int n = 10;
    int m = 100;

    DS ds(n, m);

    for(int i = 0; i < n; ++i){
        ds.insertElement(i);
    }

    cout << "-------------------" << endl;
    for(int i = 0; i < n; ++i){
        cout << i << " " << ds.search(i) << endl;
    }

    for(int i = 0; i < n; i += 2){
        ds.deleteElement(i);
    }

    cout << "-------------------" << endl;
    for(int i = 0; i < n; ++i){
        cout << i << " " << ds.search(i) << endl;
    }

    cout << "-------------------" << endl;
    ds.insertElement(1);
    ds.deleteElement(1);
    ds.deleteElement(1);
    cout << 1 << " " << ds.search(1) << endl;

    cout << "-------------------" << endl;
    for(int i = 0; i < n; ++i){
        cout << i << " " << ds.search(i) << endl;
    }

    return 0;
}
