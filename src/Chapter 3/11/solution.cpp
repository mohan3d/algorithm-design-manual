/*
 3-11. [5] Suppose that we are given a sequence of n values x 1 , x 2 , ..., x n and seek to
 quickly answer repeated queries of the form: given i and j, find the smallest value
 in x i , . . . , x j .

 (a) Design a data structure that uses O(n 2 ) space and answers queries in O(1)
 time.

 (b) Design a data structure that uses O(n) space and answers queries in O(log n)
 time. For partial credit, your data structure can use O(n log n) space and have
 O(log n) query time.
*/

#include <iostream>

using namespace std;

/* A) use matrix to store minimum element in each range(i, j)
   Construction and memory O(n^2)
*/
class MinDB{
private:
    int **min_elements;
    int size;

    void initialize(){

        min_elements = new int*[size];

        for (int i = 0; i < size; ++i) {
            min_elements[i] = new int[size];
        }
    }

    void construct_minimum(int elements[]){
        for(int i = 0; i < size; ++i)
            min_elements[i][i] = elements[i];

        for(int i = 0; i < size; ++i){
            for (int j = i + 1; j < size; ++j) {
                min_elements[i][j] = min(elements[j], min_elements[i][j - 1]);
            }
        }
    }

public:
    MinDB(int elements[], int _size){
        size = _size;
        initialize();
        construct_minimum(elements);
    }
	
    ~MinDB(){
        for(int i = 0; i < size; ++i){
            delete[] min_elements[i];
        }

        delete[] min_elements;
    }
	
    int minInRange(int i, int j) const{
        // Raise error if (i or j) < 0 or >= size

        if(i > j)
            return min_elements[j][i];

        return min_elements[i][j];
    }
};

int main() {

    const int SIZE = 10;
    int elements[SIZE] = {1, 10, 3, 11, -1, 71, 18, 20, -3, 4};

    MinDB db(elements, SIZE);
    for(int i = 0; i < SIZE; ++i){
        for(int j = i; j < SIZE; ++j){
            cout << db.minInRange(i, j) << " ";
        }
        cout << endl;
    }

    return 0;
}
