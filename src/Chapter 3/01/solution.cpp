/*
 3-1. [3] A common problem for compilers and text editors is determining whether the
 parentheses in a string are balanced and properly nested. For example, the string
 ((())())() contains properly nested pairs of parentheses, which the strings )()( and
 ()) do not. Give an algorithm that returns true if a string contains properly nested
 and balanced parentheses, and false if otherwise. For full credit, identify the position
 of the first offending parenthesis if the string is not properly nested and balanced.
 */

#include <iostream>
#include <stack>

using namespace std;

/*
 * Assume line contains only '(' and/or ')'.
 * Return -1 if a string contains properly nested and balanced parentheses, otherwise return index of the first
 * offending parenthesis.
 */

// Without stack.
int CheckParentheses1(string line){
    int count = 0;
    int possible_error_at = -1;

    for(int i = 0; i < line.length(); ++i){
        switch(line[i]){
            case '(': {
                possible_error_at = i;
                ++count;
            }
            break;

            case ')': {
                --count;
                --possible_error_at;

                if(count < 0)
                    return i;
            }
            break;

            default:break;
        }
    }

    return ((!count) ? -1 : possible_error_at);
}

// With stack.
int CheckParentheses2(string line) {
    stack<int> positions;

    for(int i = 0; i < line.length(); ++i){
        switch(line[i]){
            case '(': {
                positions.push(i);
            }
                break;

            case ')': {
                if(positions.empty())
                    return i;

                positions.pop();
            }
                break;

            default:break;
        }
    }

    return ((positions.empty()) ? -1 : positions.top());
}

int main() {

    const int CASES_SIZE = 10;
    string cases[CASES_SIZE] = {
            "(())())()",
            ")()(",
            "())",
            "(((())",
            "((())",
            "()()()()",
            "((()))",
            "(())(())()()(()",
            "(())(())()()(())(()",
            "((((((((()",
    };

    for(int i = 0; i < CASES_SIZE; ++i){
        cout << cases[i] << " " << CheckParentheses1(cases[i]) << " " << CheckParentheses2(cases[i]) << endl;
    }

    return 0;
}
